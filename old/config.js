const mongoose = require("mongoose");
const CONNECTION_URL = 'mongodb+srv://root:cem304@cluster0-tzzvp.azure.mongodb.net/test?retryWrites=true';//mongodb connection url

const options = {
    reconnectTries: Number.MAX_VALUE,
    poolSize:10
};

mongoose.connect(CONNECTION_URL,options).then(
    () =>{
        console.log("Database connection established!")
    },
    err =>{
        console.log("Error",err);
    }
);

require("../models/Task")