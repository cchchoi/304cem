//Import express
const Express = require('express');
//Import body-parser
const BodyParser = require('body-parser');
//Import api-routes.js
const apiRoutes = require("./api-routes");
//Import MongoDB
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;


//initialize the app
const app = Express();

//Use API routes in the app 
app.use('/api',apiRoutes);
app.use(BodyParser.json());
//configre bodyparser to handle post requsts
app.use(BodyParser.urlencoded({ extended:true }));

//return express when work
app.get('/',(req,res) => res.send('Hello world with Express'));

var database, collection;

//create server port and connect mongodb
app.listen(10888,() =>{
    console.log("Running 304cem on 10888");
    
})


 