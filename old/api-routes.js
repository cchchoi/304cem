let router = require('express').Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const CONNECTION_URL = 'mongodb+srv://root:cem304@cluster0-tzzvp.azure.mongodb.net/test?retryWrites=true';//mongodb connection url
const DATABASE_NAME = '304CEM';//mongodb database name

var database;

MongoClient.connect(CONNECTION_URL,{useNewUrlParser:true},(error,client)=>{
    if(error) throw error;
    database = client.db(DATABASE_NAME);
    //collection = database.collection('user');
    console.log(`Connected to ${DATABASE_NAME}!`);
})


//set default api respones
router.get('/',function(req,res){
    res.json({
        status:'API is working',
        message:'Welcome to Change. api',
    });
});

//get user
router.get('/get/user',(req,res) =>{
    console.log('someone requst to get all user from database');
    var record = database.collection('user').find();
    var result ='';
    record.forEach(function(record){
        if(record != null){
            console.log(record);
            result = result +JSON.stringify(record);
        }
    },function(err){
        if(err){
            res.status(500).send(err);
        }
        console.log(result);
        res.send(result);
    });
});

//get user 
router.get('/get/user/id/:id',function(req,res){
    var uid = ObjectId(req.params.id);
    console.log('someone requst to get all user from database'+uid);
    
    var record = database.collection('user').find({_id:uid});
    var result ='';
    record.forEach(function(record){
        if(record != null){
            console.log(record);
            result = result +JSON.stringify(record);
        }
    },function(err){
        if(err){
            res.status(500).send(err);
        }
        console.log(result);
        res.send(result);
    });
});


//insert user
router.post('/post/user',function(req,res){
    //TODO get data from html form
    var doc = {user_name:'testing',email:'testemail',pw:'testing',type:1};
    database.collection('user').insertOne(doc,function(err,obj){
        if(err) throw err;
        console.log('inserted user');
    });
});

//update user
router.put('/updata/user/id/:id',function(req,res){
    const uid = ObjectId(req.params.id);
    const newValues = {$set: req.body};
    database.collection('user').updateOne({_id:uid},newValues,function(err,obj){
        if(err) throw err;
        console.log("user updated");
        obj.json({
            status:"sucess",
            message:"user updated"
        });
    });
});

//detele user
router.delete('/delete/user/id/:id',function(req,res){
    var uid = ObjectId(req.params.id);
    database.collection('user').deleteOne({_id:uid},function(err,req){
        if(err) throw err;
        console.log(req.result.n+"has deleted");
        res.json({
            status:'sucess',
            message:req.result.n+" user has deleted"
        });
    })
});

//create prudect
router.post('/post/product',function(req,res){
    database.collection('product').insertOne({},function(err,result){
        if(err) throw err;
        res.json({
            status:'sucess',
            message: 'one product added'
        });
    });
});

//update product
router.put('/updata/product/id/:id',function(req,res){
    const uid = ObjectId(req.params.id);
    const newValues = {$set: req.body};
    database.collection('product').updateOne({_id:uid},newValues,function(err,obj){
        if(err) throw err;
        console.log(uid+"product updated");
        obj.json({
            status:"sucess",
            message:"product updated"
        });
    });
});

//deletel product
router.delete('/delete/product/id/:id',function(req,res){
    var uid = ObjectId(req.params.id);
    database.collection('product').deleteOne({_id:uid},function(err,req){
        if(err) throw err;
        console.log(uid+" has deleted");
        res.json({
            status:'sucess',
            message:req.result.n+" product has deleted"
        });
    })
});

//select product by id 
router.get('/get/product/id/:id',function(req,res){
    var uid = ObjectId(req.params.id);
    database.collection('product').find({_id:uid},function(err,result){
        if(err) throw err;
        res.json(result);
    });
})

module.exports = router;