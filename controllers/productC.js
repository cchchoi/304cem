const router = require('express').Router();
const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const CONNECTION_URL = 'mongodb+srv://root:cem304@cluster0-tzzvp.azure.mongodb.net/test?retryWrites=true';//mongodb connection url
const DATABASE_NAME = '304CEM';//mongodb database name

var database, collection;

MongoClient.connect(CONNECTION_URL,{ useNewUrlParser: true }, (err,client)=>{
    if(err) throw err;
    database = client.db(DATABASE_NAME);
    collection = database.collection("product");
    console.log(`Connected to ${DATABASE_NAME} !`);
});


exports.create = function(req,res){
    console.log("Someone want to create product");
    collection.insertOne(req.body, function(err,result){
        if(err){
            res.status(500).send({"status":500, "description":err});
        }else{
        console.log({"status":201, "description":req.body});
        res.redirect('/')
        //res.redirect('/product_detail/'+pre.body._id);
        }
    });
}

exports.list = function(req,res){
    console.log("Someone request to get all product from database");
    collection.find().toArray(function(err,result){
        if(err){
            res.status(500).send({"status":500, "description":err})
        }else{
        res.json(result);
        }
    })
}

exports.update = function(req,res){
    console.log(`Somebody try to update the product width id ${req.params.id}`)
    collection.findOneAndUpdate({_id:ObjectId(req.params.id)},{$set:req.body},{new:false},function(err){
        if(err){
            res.status(500).send({"status":500,"description":err})
        }else{
            //res.send(req.body)
        console.log({"status":201,"description":"Data update successfully"})
        res.redirect('/')
        }
    })
}

exports.delete = function(req,res){
    console.log("Someone want to delete user");
    collection.deleteOne({_id:ObjectId(req.params.id)}, function(err){
        if(err){
            res.status(500).send({"status":500, "description":err});
        }else{
        console.log(req.params.id+" product has been delete")
        res.redirect("/list")
        }
        //res.status(201).send({"status":201, "description":"Data input sucessfully"});

    });
}

//TODO: change condition
exports.searchKeyword = function(req,res){
    console.log(`Somebody looking for ${req.params.keyword}`);
    collection.find({$or:[{email:{$regex:req.params.keyword}}]}).toArray(function(err,result){
        if(err){
            res.status(500).send({"status":500,"description":err});
        }
        res.send(result);
    })
}
exports.detail = function(req,res){
    console.log(`Somebody looking for ${req.params.id}`);
    collection.find({_id:ObjectId(req.params.id) }).toArray(function(err,result){
        if(err){
            res.status(500).send({"status":500,"description":err});

        }else{
        res.json(result[0]);
        }
    })
}