const router = require('express').Router();
const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const CONNECTION_URL = 'mongodb+srv://root:cem304@cluster0-tzzvp.azure.mongodb.net/test?retryWrites=true';//mongodb connection url
const DATABASE_NAME = '304CEM';//mongodb database name


var database, collection;

MongoClient.connect(CONNECTION_URL,{ useNewUrlParser: true }, (err,client)=>{
    if(err) throw err;
    database = client.db(DATABASE_NAME);
    collection = database.collection("user");
    console.log(`Connected to ${DATABASE_NAME} !`);
});

exports.create = function(req,res){
    console.log("Someone want to create user");
    collection.insertOne(req.body, function(err,result){
        if(err){
            res.status(500).send({"status":500, "description":err});
        }
        console.log({"status":201, "description":"Data input sucessfully"});
        res.redirect('/user_login');
    });
}

exports.list = function(req,res){
    console.log("Someone request to get all books from database");
    collection.find().toArray(function(err,result){
        if(err){
            res.status(500).send({"status":500, "description":err})
        }
        res.send(result);
    })
}

exports.update = function(req,res){
    console.log(`Somebody try to update the use width id ${req.params.id}`)
    collection.findOneAndUpdate({_id:ObjectId(req.params.id)},{$set:req.body},{new:false},function(err){
        if(err){
            res.status(500).send({"status":500,"description":err})
        }else{
        console.log({"status":201,"description":"Data update successfully"})
        res.redirect('/');
        }
    })
}

//for logn
exports.login = function(req,res){
    console.log(`Somebody looking for login`);
    collection.findOne({$and:[{email:req.body.email,pw:req.body.pw}]},function(err,result){
        if(err){
            res.status(500).send({"status":500,"description":err});
        }
        if(result==null){
            res.send('no such user')
            console.log('no user'+req.body.email)
        }else{
        //TODO cookie or sessaion
        //res.send(result._id.toString())
        res.clearCookie("id");
        res.cookie("id",result._id,{ expires: new Date(Date.now() + 900000), httpOnly: true })
        res.redirect('/');
        }
    })
}

exports.delete = function(req,res){
    console.log("Someone want to delete user");
    collection.deleteOne({_id:ObjectId(req.params.id)}, function(err){
        if(err){
            res.status(500).send({"status":500, "description":err});
        }else{
        //res.status(201).send({"status":201, "description":"Data input sucessfully"});
        console.log(req.params.id+ " has been deleted")
        res.redirect('/')
        }
    });
}

exports.search = function(req,res){
    console.log(`Somebody looking for ${req.params.id}`);
    collection.find({_id:ObjectId(req.params.id) }).toArray(function(err,result){
        if(err){
            res.status(500).send({"status":500,"description":err});
        }else{
        res.json(result[0]);
        }
    })
}