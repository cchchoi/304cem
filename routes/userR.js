const Express = require('express');
const Route = Express.Router();
const path = require('path');
const user_controller = require('../controllers/userC');

const {validate, VaildationError} = require('express-json-validator');
const userSchema = require('../models/userM')


//create user
Route.post('/user/create',user_controller.create)
//Sign up page 
Route.get('/user_create',function(req,res){
    res.sendFile(path.join(__dirname,'../www/sign_up.html'))
});
//Route.get('/user',user_controller.test);
Route.get('/list',user_controller.list);
Route.post('/user/update/id/:id',user_controller.update)
Route.get('/user_update',function(req,res){
    res.sendFile(path.join(__dirname,'../www/profile.html'))
});
Route.get('/user/id/:id',user_controller.search)
Route.get('/user_login',function(req,res){
    res.sendFile(path.join(__dirname,'../www/login.html'))
});
Route.post('/user/login',user_controller.login)
//delete
Route.post('/user/delete/:id',user_controller.delete)

module.exports = Route;