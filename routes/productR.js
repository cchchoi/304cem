const Express = require('express');
const Route = Express.Router();
const path = require('path');
const product_controller = require('../controllers/productC');

const {validate, VaildationError} = require('express-json-validator');
const userSchema = require('../models/productM')

//create product
Route.post('/product/create',product_controller.create)
//create product page 
Route.get('/product_create',function(req,res){
    res.sendFile(path.join(__dirname,'../www','product_add.html'))
});
//Route.get('/user',user_controller.test);
Route.get('/product/list',product_controller.list);
Route.get('/product_list',function(req,res){
    res.sendFile(path.join(__dirname,'../www','list.html'))
})
Route.post('/product/update/:id',product_controller.update)
Route.get('/product_update',function(req,res){
    res.sendFile(path.join(__dirname,'../www','product_detail.html'))
})
Route.get('/list/:keyword',product_controller.searchKeyword)
Route.post('/product/delete/:id',product_controller.delete)
Route.get('/product/id/:id',product_controller.detail)
Route.get('/categories',function(req,res){
    res.sendFile(path.join(__dirname,'../www','categories.html'))
})
Route.get('/product',function(req,res){
    res.sendFile(path.join(__dirname,'../www','product.html'))
})
Route.get('/product_detail/:id',function(req,res){
    res.sendFile(path.join(__dirname,'../www','product_detail.html'))
})
Route.get('/product_info',function(req,res){
    res.sendFile(path.join(__dirname,'../www','product_info.html'))
})
module.exports = Route;