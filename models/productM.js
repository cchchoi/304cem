exports.userSchema ={
    type:'object',
    required: ["name","pw"],
    properties:{
        user_name:{
            type:"string"
        },
        pw:{
            type:"string"
        }
    }
}