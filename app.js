const Express = require('express');
const BodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override')

const App = Express();

App.use(BodyParser.json());
App.use(BodyParser.urlencoded({ extended:true }));
App.use(methodOverride('_method'))

//return express when work
App.use('/',require('./index'))
App.use('/',require('./routes/userR'))
App.use(cookieParser());
//App.use('/cookie' ,require('./routes/loginApi'))
App.use('/',require('./routes/productR'))

App.use(Express.static(__dirname+'/www'));
App.use(Express.static('routes'))
//App.use('/user',Express.static('user'))


const port = 10880;

App.listen(port,()=>{
    console.log(`server is running on port number ${port}`);
})